import xlrd

from manage import db, User, Book

Book.query.delete()
db.session.commit()


n = 1
def nextID(id_):
	global n
	try:
		if(n == int(id_)):
			n += 1
			return True
	except Exception, e:
		return False
	finally:
		pass
	return False

rb = xlrd.open_workbook('books.xls', formatting_info = True)
sheet = rb.sheet_by_index(0)

mode = False
keys = []
count = 0
MAX_COUNT = 13

def genScan(id_):
	uid = str(id_)
	n = 6 - len(uid)
	return "1852" + ("0" * n) + uid

def parseArray(key):
	global n

	# DEBUG #
	if(key[10] == ""):
		key[10] = 1
	else:
		key[10] = int(key[10])
	##########

	# SCAN #
	if(key[12] == ""):
		key[12] = genScan(str(n - 1))
	########
	newUser = Book(
		name_author = key[1],
		sur_author = key[0],
		middle_author = key[2],
		title = key[3],
		category = key[4],
		years = key[5],
		publisher_company = key[6],
		count_pages = key[7],
		description = key[8],
		full_description = key[9],
		count_books = key[10],
		mark = int(key[11]),
		scan = key[12]
	)

	db.session.add(newUser)
	try:
	    db.session.commit()
	except:
		db.session.rollback()

for rownum in range(sheet.nrows):
	row = sheet.row_values(rownum)
	for el in row:
		if(mode and count == MAX_COUNT):
			mode = False
			count = 0
			
			parseArray(keys)

			keys = []
		if(mode):
			count += 1
			keys.append(el)
		if(not mode and nextID(el)):
			print("Parse: " + el)
			mode = True
