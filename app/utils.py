import datetime

def isInt(el):
	try:
		el = int(el)
	except Exception, e:
		return False
	
	return True

def lastDay(days):
	today = datetime.date.today()
	last = datetime.timedelta(days = need_days)
	return today - days

def deltaDays(book_passed, need_days):
	last = lastDay(need_days)
	return book_passed.date() - last

def getTime():
	return datetime.datetime.now()

def genScan(id_):
	uid = str(id_)
	n = 4 - len(uid)
	return "1852" + ("0" * n) + uid