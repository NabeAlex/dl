from flask import render_template, request, session, redirect, url_for, jsonify
from app import app
from forms import *

from manage import db, User, Book, BooksForPupil

from utils import *
from models import *

@app.route('/')
def empty():
	return index("")

@app.route('/<path>')
def index(path):
	if(path in redirectMap):
		path = redirectMap[path]

	user = None
	mode = 0
	if("id" in session):
		user = checkUser(session["id"])
		if(user):
			mode = getMode(user.role)
		else:
			user = None
			session.clear()
			path = "welcome"

	if(path in hashMap):
		tmp = hashMap[path]
		# FOR WELCOME
		if(mode > 0 and (tmp.mode == 0 and not tmp.high)):
			path = "cabinet"
			return redirect(url_for('index', path = path))

		if((tmp.mode < mode and tmp.high) or (tmp.mode == mode)):
			# SUCCESS
			return getTemplate(toPath(path), user)
		else:
			return getTemplate(toPath("notavaible"), user)
	else:
		return getTemplate(toPath("404"))
	
	return getTemplate(toPath("welcome"), user)

@app.route('/signup', methods = ['GET', 'POST'])
def signup():
	type = request.args.get('t', '')
	isPOST = (request.method == 'POST')

	result = "Error"
	if(type == "teacher"):
		result = login(request.form, type) if (isPOST) else authField(type)
	elif(type == "librarian"):
		pass
	else:
		result = login(request.form, type) if (isPOST) else authField(type)
	return result

@app.route('/auth')
def auth():
	q = request.args.get('q', '')
	return render_template(toPath("form"), title = "Authorization", content = "auth.html", message = q)

@app.route('/user/<username>')
def profile(username): 
	if("role" in session):
		mode = getMode(session["role"])
		if(not checkPerm("user", mode)):
			return username
	else:
		return username

	if(username == "all"):
		return render_template("index.html", title = "Digital library | ALL", 			\
			includes_blocks = [], 													    \
			content = "user.html", one = False, users = User.query.all())
	else:
		user = User.query.filter_by(scan = username).first()
		b = BooksForPupil.query.filter_by(p_id = user)
		bs = []
		for i in b:
			bs.append(i.b_id)

		if(user):
			return render_template("index.html", title = "Digital library | " + username, 			\
					includes_blocks = [],															\
					content = "user.html", one = True, user = user, 								\
													   books = bs)

	return username

@app.route('/about')
def about():
	return render_template("index.html", title = "Digital library | About", \
		includes_blocks = [], 												\
		content = "about.html")

def authField(type):
	return render_template(toPath("form"), title = "Sign up", content = "signup.html", who = type, form = LoginForm())

####################
#                  #
#	POST Methods   #
#                  #
####################


def login(f, who):
	name, surName, date, class_, number,     \
	email, phone  =				             \
		    f.get('name', ''),               \
		 	f.get('last_name', ''),          \
		 	f.get('date', ''),               \
		 	f.get('select_class', ''),       \
		 	f.get('number_class', ''),       \
		 	f.get('email', ''),              \
			f.get('phone', '')

	uid = generateUID(name, surName, date)
	
	newUser = User(
		uid = uid,
		name = name,
		sur_name = surName,
		years = date,
		class_ = class_,
		number = number,
		email = email,
		phone = phone,
		role = who
	)

	db.session.add(newUser)

	try:
	    db.session.commit()
	    newUser.scan = genScan(newUser.id)
	    db.session.commit()
	except:
		db.session.rollback()

	return redirect(url_for('index', path = ""))

@app.route('/login', methods = ["POST"])
def inServer():
	login = request.form.get('login', '')
	password = request.form.get('password', '')

	user = User.query.filter(User.uid == login);

	if(0 < user.count() <= 1 and len(password) > 2):
		if(User.isPassword(user[0], password)):
			session["id"] = user[0].id
			session["role"] = user[0].role
			return redirect(url_for('index', path = "cabinet"))
	elif(user.count() > 1):
		return "Call teacher!"

	return redirect(url_for('auth', q = "Wrong login or password"))

@app.route('/loginout', methods = ["POST"])
def outServer():
	session.clear()
	return redirect(url_for('index', path = "welcome"))

# LOGIC #

def getTemplate(component, user = None):
	if(user == None):
		return render_template("index.html", title = "Digital library", \
			includes_blocks = generateLeftBlocks(),   \
			content = component)

	return render_template("index.html", title = "Digital library", \
		user = user, \
		includes_blocks = generateLeftBlocks(user),   \
		content = component)	


#########
	
# AJAX #

@app.route("/ajax/user")
def ajaxUser():
	scan = getScan(request.args)

	if(scan != ''):
		user = User.query.filter_by(scan = scan).first()
		if(user):
			return jsonify({"status": "OK"})
	return jsonify(error())

@app.route("/ajax/book")
def ajaxBook():
	scan = getScan(request.args)

	if(scan != ''):
		book = Book.query.filter_by(scan = scan).first()
		if(book):
			return jsonify({"status": "OK", "data": {"id": book.id, "name": book.sur_author, "title": book.title}})
	return jsonify(error())

@app.route("/ajax/push")
def pushBook():
	scan = request.args.get('to', '')
	
	try:
		user = User.query.filter_by(scan = scan).first()
	except Exception, e:
		return jsonify(error())
	
	try:
		n = int(request.args.get('n', ''))
		id_user = int(request.args.get('to', ''))
	except Exception, e:
		return jsonify(error())

	if(n < 10):
		for i in range(n):
			tmp = request.args.get("b" + str(i), '')
			if(tmp != '' and isInt(tmp)):
				book = getBook(int(tmp))
				if(book):
					t = getTime()
					newPass = BooksForPupil(p_id = user, b_id = book, time_pass = 14, time = t)
					db.session.add(newPass)
				else:
					break
			else:
				break
		else:
			db.session.commit()
			return jsonify({"status": "OK"})
		return jsonify(error())

@app.route('/ajax/setbook')
def setBook():
	scan = getScan(request.args)
	try:
		days = int(request.args.get('days', ''))
	except Exception, e:
		return jsonify(error())
	
	if(scan != ''):
		book = Book.query.filter_by(scan = scan).first()
		if(book):
			book.time_pass = days
			db.session.commit()
			return jsonify({"status": "OK"})

	return jsonify(error())
########

