$(function() {
	var id = 0
	var scan = $("#scan-user")

	scan.focus()

	scan.focusout(function() {
		if($("#front").css("background-color") == "rgb(204, 204, 204)")
			return
		
		$("#front").css("cursor", "pointer")
		$("#front").css("background-image", 'url("./static/image/refresh.png")');

		$("#scan-text").html("Нажмите для сканирования")
	
	});

	$("#front").click(function() {
		scan.focus()

		$("#scan-text").html("Приложите штрихкод")

		$("#front").css("cursor", "default")
		$("#front").css("background-image", 'url("./static/image/search.png")');
	});

	scan.change(function() {
		var infoUser = scan.val()
		scan.val("")
		$.ajax({
		  dataType: "json",
		  url: "/ajax/user?scan=" + infoUser,
		  data: {},
		  success: function(data) {
		  	if(data["status"] == "OK")
		  		window.location.href = '/user/' + infoUser
		  }
		});
	});

});