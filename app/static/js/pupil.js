info_id = []
var n = -1;

scan = null

function setScan(e) {
	scan = $("#scan-user-" + e)

	setTimeout(function() {
		scan.focus()
	}, 1000)

	$("#accept-scan").click(accept)
	scan.change(function() {
		var infoBook = scan.val()
		scan.val("")
		$.ajax({
		  dataType: "json",
		  url: "/ajax/book?scan=" + infoBook,
		  data: {},
		  success: function(data) {
		  	if(data["status"] == "OK") {
		  		nextID({id: data["data"]["id"], category: data["data"]["name"], sur_name: data["data"]["title"]})
		  	}
		  }
		});

	});

	$("#front-" + e).click(function() {
		scan.focus()

		$("#scan-text-" + e).html("Приложите штрихкод")

		$("#front-" + e).css("cursor", "default")
		$("#front-" + e).css("background-image", 'url("/static/image/search.png")');
	});

	scan.focusout(function() {
		if($("#front-" + e).css("background-color") == "rgb(204, 204, 204)")
			return

		$("#front-" + e).css("cursor", "pointer")
		$("#front-" + e).css("background-image", 'url("/static/image/refresh.png")');

		$("#scan-text-" + e).html("Нажмите для сканирования")
	
	});

}

function setDelete(e) {

}

function unbind(e) {
	$("#front-" + e).unbind("click");

	$("#front-" + e).css("cursor", "default")
	
	scan.off("change")
	scan.off("focusout")
}

function nextID(info) {
	if(n >= 0) {
		info_id.push(info.id)
		
		unbind(n)

		$('#front-' + n).css("background-image", 'url("/static/image/book.png")')
		$('#front-' + n).css("background-size", '10px 50px")')
		$('#front-' + n).css("background-position", 'left')

		$('#front-text-' + n).html(info.sur_name + "(" + info.category + ")")
		$('#scan-text-' + n).html("")
	}
	n++;
	$("#group-scan").html($("#group-scan").html() + '<div id="scan-' + n + '" class="scan"><div id="front-' + n + '" class="front"><div id="front-text-' + n + '" class="front-text"></div></div><input id="scan-user-' + n + '" type="text" class="scan-user input-scan" /><div id="scan-text-' + n + '" class="scan-text">Приложите штрихкод</div></div>')
	
	/* ACCEPT */
	if(n == 1) {
		$("#group-scan").html("<input type='submit' id='accept-scan' value='Взять' style='width: 100%;' /> " + $("#group-scan").html())
	}

	setScan(n);
}

$(function() {
	var id = 0
	nextID();
});

accept = function() {
	var n = info_id.length
	var tmp = window.location.href.split("/")
	var to = tmp[tmp.length - 1]

	var req = "?n=" + n + "&to=" + to
	for(var i = 0; i < n; i++) {
		req += "&b" + i + "=" + info_id[i] 
	}

	$.ajax({
	  dataType: "json",
	  url: "/ajax/push" + req,
	  data: {},
	  success: function(data) {
	  	if(data["status"] == "OK")
	  		window.location.reload();
	  }
	});
}