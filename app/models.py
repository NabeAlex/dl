from manage import User, Book

def toPath(path):
	return path + ".html"

def getScan(typeRequest):
	return typeRequest.get("scan", '')

def error():
	return {"status": "Error"}
	
class Component:

	def __init__(self, mode, andHigh = True, pathTrue = ""):
		self.mode = mode
		self.high = andHigh

# 0 - NEW_USER
# 1 - PUPIL
# 2 - TEACHER
# 3 - LIBRARIAN
# 4 - ADMIN

hashMap = {
	# NEW #
	"books"      : Component(0),
	"news"       : Component(0),
	"info"       : Component(0),
	"blog"       : Component(0),

	"welcome"    : Component(0, False),
	"why"        : Component(0, False),
	"who"        : Component(0, False),
	"begin"      : Component(0, False),
	"about"      : Component(0, False),

	# PUPIL #
	"message"    : Component(1),
	"letter"     : Component(1),
	"cabinet"    : Component(1),

	"mybook"     : Component(1, False),
	"myteacher"  : Component(1, False),

	# TEACHER #
	"mypupil"    : Component(2, False),

	# PUPIL SEE YOUR PUPLS

	# LIBRARIAN #
	"workplace"  : Component(3, False),
	"debtors"    : Component(3, False),
	"user"      : Component(3, False),
	"questions"  : Component(3, False) 
}

# REDIRECT #
redirectMap = {}

redirectMap[""] = "welcome" 

############

def getMode(s):
	if(s == "pupil"):
		return 1
	elif(s == "teacher"):
		return 2
	elif(s == "librarian"):
		return 3
	elif(s == "admin"):
		return 4
	else:
		return 0

def checkPerm(path, mode):
	perm = hashMap[path]
	if(perm.high and mode >= perm.mode):
		return True
	if(not perm.high and mode == perm.mode):
		return True
	return False 
# SCAN - 13

def generateUID(name, surName, date):
	new_date = date.split("-")
	#for i in range(len(new_date)):
	#	new_date[i] = new_date[i][::-1]
	new_date.reverse()
	new_date = '-'.join(new_date)
	return (name[0].upper() + surName[0].upper() + "-" + new_date)

def getCurrentUser(id_):
	return User.query.filter_by(id = id_).first()

def getBook(id_):
	return Book.query.filter_by(id = id_).first()

def checkUser(id_):
	user = User.query.filter(User.id == id_)
	if(user.count() != 1):
		return False

	return user.first()

def generateLeftBlocks(user = ''):
	if(type(user) == str):
		return ["block_auth.html", "block_menu.html"]

	if(user.role == "pupil"):
		return ["block_info.html"]
	elif(user.role == "teacher"):
		return ["block_info.html"]
	elif(user.role == "librarian"):
		return ["block_info.html"]
	else:
		return ["block_auth.html", "block_menu.html"]
