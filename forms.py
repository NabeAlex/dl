from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, SelectField, DateField
from wtforms.validators import Required, Length

class LoginForm(Form):
    name = TextField('name')
    last_name = TextField('last_name')

    date = DateField('date', format='%Y.%m.%d')


    classes = [("", "")]
    for i in range(1, 12):
    	classes.append((i, i))


    select_class = SelectField('class', default = 0, choices = classes)

    # -*- coding: utf-8 -*-
    number_class = SelectField('number', default = 0, choices = [("", ""), ("a", u'\u0410'), ("b", u'\u0411'), ("c", u'\u0412'), \
    																				     ("d", u'\u0413'), ("e", u'\u0414')])

    email = TextField('email')
    phone = TextField('phone')

